<div class="send-container">
  <div class="send-header">
    <h2 class="header-text" id="send-medical-head"><img id="send-icon" src="images/icon-send.png"></img>Send Images</h2>
  </div>
  <div class="send-confirmation-head">
    <h2 class="confirm-head">Confirmation</h2>
  </div>
  <div class="send-confirmation-message">
    <h5 class="confirm-message-head">Thank you</h5>
    <p class="confirm-message-body">Transaction #21 has been submitted.</p>
  </div>
</div>